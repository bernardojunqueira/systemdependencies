# System Dependencies Manager

This is a System Dependencies Manager, used to install and remove system components and their dependencies.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Visual Studio 2017 with .NET desktop development workload.

### Installing

Install Visual Studio 2017 and select .NET desktop development workload during the installation process.

## Deployment

Open the Solution file (SystemDependencies.sln) in Visual Studio 2017 and press CTRL+F5 to build the project and generate the executable file.

## Authors

* **Bernardo Junqueira** - *Initial work*

See also the list of [contributors](https://bitbucket.org/bernardojunqueira/systemdependencies) who participated in this project.

## License

I decided to use the MIT License because is short and to the point. It lets people do almost anything they want with your project, including to make and distribute closed source versions.

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
