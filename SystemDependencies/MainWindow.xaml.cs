﻿// ******** Group 5 Members ********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************

using Microsoft.Win32;
using System.Windows;
using SystemDependencies.ViewModels;

namespace SystemDependencies
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowViewModel vm = new MainWindowViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void OnOpenButtonClicked(object sender, RoutedEventArgs e)
        {
            var openfileDialog = new OpenFileDialog
            {
                Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*"
            };

            var dialogResult = openfileDialog.ShowDialog();
            if (dialogResult == true)
            {
                vm.OpenFile(openfileDialog.FileName);
            }
        }

        private void OnDoItButtonClicked(object sender, RoutedEventArgs e)
        {
            vm.DoIt();
        }

        private void OnSaveButtonClicked(object sender, RoutedEventArgs e)
        {
            var savefileDialog = new SaveFileDialog
            {
                Filter = "Text Files (*.txt)|*.txt"
            };

            var dialogResult = savefileDialog.ShowDialog();
            if (dialogResult == true)
            {
                vm.SaveFile(savefileDialog.FileName);
            }
        }
    }
}
