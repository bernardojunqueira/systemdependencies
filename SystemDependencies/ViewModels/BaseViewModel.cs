﻿// ******** Group 5 Members ********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SystemDependencies.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members 

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        protected virtual void NotifyPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}


