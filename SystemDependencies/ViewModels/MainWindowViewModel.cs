﻿// ******** Group 5 Members ********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using SystemDependencies.Models;

namespace SystemDependencies.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        const int MAX_CHARACTERS_PER_LINE = 80;
        const int MAX_CHARACTERS_PER_WORD = 10;
        const string DEPEND_COMMAND = "DEPEND";
        const string INSTALL_COMMAND = "INSTALL";
        const string LIST_COMMAND = "LIST";
        const string REMOVE_COMMAND = "REMOVE";
        
        public string Input { get; set; }
        public string Output { get; set; }
        private string[] fileLines { get; set; }
        private List<Component> components { get; set; } = new List<Component>();

        public void DoIt()
        {
            Output = string.Empty;
            components.Clear();

            foreach (string line in fileLines)
            {
                if (line.Length <= MAX_CHARACTERS_PER_LINE)
                {
                    var command = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).First();
                    var components = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();

                    if (command.Length <= MAX_CHARACTERS_PER_WORD)
                        switch (command)
                        {
                            case DEPEND_COMMAND:
                                Output += depend(components);
                                break;
                            case INSTALL_COMMAND:
                                Output += install(components.First());
                                break;
                            case LIST_COMMAND:
                                Output += list();
                                break;
                            case REMOVE_COMMAND:
                                Output += remove(components.First());
                                break;
                            default:
                                Output += command;
                                break;
                        }
                }
            }
            NotifyPropertyChanged(nameof(Output));
        }

        public void OpenFile(string path)
        {
            try
            {
                fileLines = File.ReadAllLines(path);
                Input = string.Join(Environment.NewLine, fileLines);
                NotifyPropertyChanged(nameof(Input));
            }
            catch (Exception ex)
            {
                Output += $"An error occurred while reading the text file: {ex.Message}";
            }
        }

        public void SaveFile(string path)
        {
            try
            {
                if (Output.Any())
                    File.WriteAllText(path, Output);
            }
            catch (Exception ex)
            {
                Output += $"An error occurred while saving the text file: {ex.Message}";
            }
        }

        private string depend(string[] components)
        {
            this.components.Add(new Component { Name = components.First(), Dependencies = components.Skip(1).ToArray(), Installed = false });
            return $"{DEPEND_COMMAND} {string.Join(" ", components)}{Environment.NewLine}";
        }

        private string install(string componentName)
        {
            var output = string.Empty;
            var component = components.SingleOrDefault(c => c.Name == componentName);
            // get calling method name
            string methodName = new StackFrame(1).GetMethod().Name;

            if (methodName != nameof(install))
            {
                output += $"{INSTALL_COMMAND} {componentName}{Environment.NewLine}";
                if (component != null) component.UserInstalled = true;
            }

            if (component != null)
            {
                if (!component.Installed)
                {
                    if (component.Dependencies != null)
                        foreach (var dependency in component.Dependencies)
                            output += install(dependency);

                    component.Installed = true;
                    output += $"\tInstalling {component.Name}{Environment.NewLine}";
                }
                else if (methodName != nameof(install)) output += $"\t{component.Name} is already installed.{Environment.NewLine}";
            }
            else
            {
                components.Add(new Component { Name = componentName, Installed = true, UserInstalled = methodName != nameof(install) ? true : false });
                output += $"\tInstalling {componentName}{Environment.NewLine}";
            }
            return output;
        }

        private string list()
        {
            return $"{LIST_COMMAND}{Environment.NewLine}\t{string.Join(Environment.NewLine + "\t", components.FindAll(c => c.Installed == true).Select(c => c.Name).ToArray())}{Environment.NewLine}";
        }

        private string remove(string componentName)
        {
            var output = string.Empty;
            var component = components.SingleOrDefault(c => c.Name == componentName);
            // get calling method name
            string methodName = new StackFrame(1).GetMethod().Name;

            if (methodName != nameof(remove)) output += $"{REMOVE_COMMAND} {componentName}{Environment.NewLine}";

            if (component != null && component.Installed)
            {
                if (!components.Any(c => c.Installed && c.Dependencies != null && c.Dependencies.Contains(component.Name)))
                {
                    if ((component.UserInstalled && methodName != nameof(remove)) || !component.UserInstalled)
                    {
                        component.Installed = false;
                        output += $"\tRemoving {component.Name}{Environment.NewLine}";

                        if (component.Dependencies != null)
                            foreach (var dependency in component.Dependencies.Reverse())
                                output += remove(dependency);
                    }                   
                }
                else if (methodName != nameof(remove)) output += $"\t{component.Name} is still needed.{Environment.NewLine}";
            }
            else
                output += $"\t{componentName} is not installed.{Environment.NewLine}";

            return output;
        }

        #region old methods
        private string install_old(string componentName, bool isExplicit = true)
        {
            var output = string.Empty;

            if (isExplicit) output += $"{INSTALL_COMMAND} {componentName}{Environment.NewLine}";

            var component = components.SingleOrDefault(c => c.Name == componentName);

            if (component != null)
            {
                if (!component.Installed)
                {
                    if (component.Dependencies != null)
                        foreach (var dependency in component.Dependencies)
                            output += install_old(dependency, false);

                    component.Installed = true;
                    output += $"\tInstalling {component.Name}{Environment.NewLine}";
                }
                else if (isExplicit) output += $"\t{component.Name} is already installed.{Environment.NewLine}";
            }
            else
            {
                components.Add(new Component { Name = componentName, Installed = true, UserInstalled = true });
                output += $"\tInstalling {componentName}{Environment.NewLine}";
            }
            return output;
        }
        private string remove_old(string componentName, bool isExplicit = true)
        {
            var output = string.Empty;

            if (isExplicit) output += $"{REMOVE_COMMAND} {componentName}{Environment.NewLine}";

            var component = components.SingleOrDefault(c => c.Name == componentName);

            if (component != null && component.Installed)
            {
                if (!components.Any(c => c.Installed && c.Dependencies != null && c.Dependencies.Contains(component.Name)))
                {
                    component.Installed = false;
                    output += $"\tRemoving {component.Name}{Environment.NewLine}";

                    if (component.Dependencies != null)
                        foreach (var dependency in component.Dependencies.Reverse())
                            output += remove_old(dependency, false);
                }
                else if (isExplicit) output += $"\t{component.Name} is still needed.{Environment.NewLine}";
            }
            else
                output += $"\t{componentName} is not installed.{Environment.NewLine}";

            return output;
        }
        #endregion
    }
}
