﻿// ******** Group 5 Members ********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************

namespace SystemDependencies.Models
{
    public class Component
    {
        public string Name { get; set; }
        public string[] Dependencies { get; set; }
        public bool Installed { get; set; }
        public bool UserInstalled { get; set; }
    }
}
